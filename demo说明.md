Springboot+SpringData+jpa实现多数据源配置（MySQL&Oracle）

一、添加依赖

二、配置文件application.yml

三、数据源配置类
    DataSourceConfig.java
    MasterDataSourceConfig.java
    SlaveDataSourceConfig.java
    配置各个数据源的dao(repository)、entity所在目录

四、创建controller、service、dao、entity,并添加对应注解

    dao:@Repository

    entity:@Entity