package com.yanpan.jpademo.datasourceone.dao;

import com.yanpan.jpademo.datasourceone.entity.User1;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;


@Repository
public interface User1Dao extends JpaRepositoryImplementation<User1,Long> {
}
