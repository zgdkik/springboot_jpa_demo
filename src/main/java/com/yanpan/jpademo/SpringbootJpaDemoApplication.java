package com.yanpan.jpademo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.yanpan.jpademo")
public class SpringbootJpaDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootJpaDemoApplication.class, args);
    }

}
