package com.yanpan.jpademo.datasourceguli.service.impl;


import com.yanpan.jpademo.datasourceguli.dao.EduCourseDao;
import com.yanpan.jpademo.datasourceguli.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
@Service
public class EduCourseServiceImpl implements EduCourseService {

    @Autowired
    private EduCourseDao eduCourseDao;

    @Override
    public List<Object> getInnerConnectData() {
        List<Object> list = eduCourseDao.getInnerConnectData();
        return list;
    }

    @Override
    public void editCourseById(String id, String title) {
        eduCourseDao.editCourseById(id,title);
    }
}
