package com.yanpan.jpademo.datasourceguli.service;


import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
public interface EduCourseService {

    List<Object> getInnerConnectData();

    void editCourseById(String id, String title);
}
