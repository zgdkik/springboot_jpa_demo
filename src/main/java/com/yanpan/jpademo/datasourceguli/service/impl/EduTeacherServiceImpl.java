package com.yanpan.jpademo.datasourceguli.service.impl;

import com.yanpan.jpademo.datasourceguli.service.EduTeacherService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
@Service
public class EduTeacherServiceImpl implements EduTeacherService {

}
