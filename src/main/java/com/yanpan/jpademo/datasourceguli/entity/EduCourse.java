package com.yanpan.jpademo.datasourceguli.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * <p>
 * 课程
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "edu_course")
public class EduCourse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    @Column(name = "teacher_id")
    private String teacherId;

    @Column(name = "subject_id")
    private String subjectId;

    @Column(name = "subject_parent_id")
    private String subjectParentId;

    @Column(name = "title")
    private String title;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "lesson_num")
    private Integer lessonNum;

    @Column(name = "cover")
    private String cover;

    @Column(name = "buy_count")
    private Long buyCount;

    @Column(name = "view_count")
    private Long viewCount;

    @Column(name = "version")
    private Long version;

    @Column(name = "status")
    private String status;

    @Column(name = "is_deleted")
    private Integer isDeleted;

    @Column(name = "gmt_create")
    private Date gmtCreate;

    @Column(name = "gmt_modified")
    private Date gmtModified;


}
