package com.yanpan.jpademo.datasourceguli.entity;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

/**
 * <p>
 * 讲师
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@Table(name = "edu_teacher")
public class EduTeacher implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "intro")
    private String intro;

    @Column(name = "career")
    private String career;

    @Column(name = "level")
    private Integer level;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "sort")
    private Integer sort;

    @Column(name = "is_deleted")
    private Boolean isDeleted;

    @Column(name = "gmt_create")
    private Date gmtCreate;

    @Column(name = "gmt_modified")
    private Date gmtModified;

}
