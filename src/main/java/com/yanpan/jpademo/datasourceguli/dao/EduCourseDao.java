package com.yanpan.jpademo.datasourceguli.dao;

import com.yanpan.jpademo.datasourceguli.entity.EduCourse;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface EduCourseDao extends JpaRepositoryImplementation<EduCourse,String> {

    //?1 代表第一个参数 以此类推 顺序不能错
//    @Query("select u from User u where u.gender = ?1 and u.age = ?2 and u.username like %?3%")
//    List<User> query(Integer gender, Integer age, String username);

    //query2()是query()的另外以一种写法 和query()相比 顺序无所谓，因为指定了参数名
//    @Query("select u from User u where u.gender = :gender and u.age = :age and u.username like %:username%")
//    List<User> query2(@Param("gender") Integer gender,
//                      @Param("age") Integer age,
//                      @Param("username") String username);

//    @Query("select EduTeacher,EduCourse from EduTeacher inner join EduCourse on EduTeacher.id = EduCourse.teacherId")
    //nativeQuery = true时，是可以执行原生SQL语句，所谓原生SQL，也就是说这段SQL拷贝到数据库中，然后把参数值给OK了，
    //查找对应的是数据库而不是实体类
    @Query(value="select t.id t_id,t.name,c.id c_id,c.title from edu_teacher t inner join edu_course c on t.id = c.teacher_id",nativeQuery = true)
    List<Object> getInnerConnectData();

    @Query(value="update edu_course c set c.title = ?2,c.price = 0.02 where id = ?1",nativeQuery = true)
    @Modifying
    @Transactional
    void editCourseById(String id, String title);
}
