package com.yanpan.jpademo.service.impl;

import com.yanpan.jpademo.datasourceone.dao.User1Dao;
import com.yanpan.jpademo.datasourceone.entity.User1;
import com.yanpan.jpademo.datasourcetwo.dao.User2Dao;
import com.yanpan.jpademo.datasourcetwo.entity.User2;
import com.yanpan.jpademo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private User1Dao userMasterDao;

    @Autowired
    private User2Dao userSlaveDao;

    @Override
    public List<User1> getMasterAll() {
        return userMasterDao.findAll();
    }

    @Override
    public List<User2> getSlaveAll() {
        return userSlaveDao.findAll();
    }
}
