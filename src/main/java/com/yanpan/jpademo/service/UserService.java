package com.yanpan.jpademo.service;

import com.yanpan.jpademo.datasourceone.entity.User1;
import com.yanpan.jpademo.datasourcetwo.entity.User2;

import java.util.List;

public interface UserService {
    List<User1> getMasterAll();

    List<User2> getSlaveAll();
}
