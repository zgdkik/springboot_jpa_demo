package com.yanpan.jpademo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * 数据源配置类
 */
@Configuration
public class DataSourceConfig {

    /**
     * 创建名为 masterDataSource 的数据源
     */
    @Primary
    @Bean(name = "masterDataSource")
    //application.yml中的master数据源
    @ConfigurationProperties(prefix = "spring.datasource.master")
    public DataSource masterDataSource() {
        return DataSourceBuilder.create().build();
    }


    /**
     * 创建名为 slaverDataSource 的数据源
     */
    @Bean(name = "slaverDataSource")
    //application.yml中的slaver数据源
    @ConfigurationProperties(prefix = "spring.datasource.slaver")
    public DataSource slaverDataSource() {
        return DataSourceBuilder.create().build();
    }

    /**
     * 创建名为 slaverDataSource 的数据源
     */
    @Bean(name = "guliDataSource")
    //application.yml中的slaver数据源
    @ConfigurationProperties(prefix = "spring.datasource.guli")
    public DataSource guliDataSource() {
        return DataSourceBuilder.create().build();
    }

}

