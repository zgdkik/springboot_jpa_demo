package com.yanpan.jpademo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateProperties;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateSettings;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.util.Map;
import java.util.Objects;

/**
 * 从数据源配置，和主数据源的配置几乎一样，区别在于没有@Primary注解
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryGuli",//注意名称和主数据源不一样
        transactionManagerRef = "transactionManagerGuli",//注意名称和主数据源不一样
        basePackages = {"com.yanpan.jpademo.datasourceguli.dao"}//配置从dao(repository)所在目录
)
public class GuliDataSourceConfig {

    @Autowired
    //DataSourceConfig中注册的bean
    @Qualifier("guliDataSource") //注意指定从数据源
    private DataSource dataSource;
    @Autowired
    private JpaProperties jpaProperties;
    @Autowired
    private HibernateProperties hibernateProperties;
    /*
    //数据库方言，多数据源是同一个数据库就不需要设置，如果不同就需要设置
    @Value("${spring.jpa.hibernate.secondary-dialect}")
    private String secondaryDialect;
    */


    @Bean("entityManagerGuli")
    public EntityManager entityManager(EntityManagerFactoryBuilder builder) {
        return Objects.requireNonNull(localContainerEntityManagerFactoryBean(builder).getObject()).createEntityManager();
    }

    @Bean("entityManagerFactoryGuli")
    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean(EntityManagerFactoryBuilder builder) {
        return builder.dataSource(dataSource)
                .properties(getVendorProperties())
                //设置实体类所在目录
                .packages("com.yanpan.jpademo.datasourceguli.entity")
                //持久化单元名称，当存在多个EntityManagerFactory时，需要制定此名称
                .persistenceUnit("guliPersistenceUnit")
                .build();
    }

    private Map<String, Object> getVendorProperties() {
        /*
        Map<String,String> map = new HashMap<>();
        map.put("hibernate.dialect",secondaryDialect);// 设置对应的数据库方言
        jpaProperties.setProperties(map);
        */
        return hibernateProperties.determineHibernateProperties(
                jpaProperties.getProperties(),
                new HibernateSettings()
        );
    }

    @Bean("transactionManagerGuli")
    public PlatformTransactionManager transactionManager(EntityManagerFactoryBuilder builder) {
        return new JpaTransactionManager(Objects.requireNonNull(localContainerEntityManagerFactoryBean(builder).getObject()));
    }

}

