package com.yanpan.jpademo.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
@RestController
@RequestMapping("/demo/edu-teacher")
public class EduTeacherController {

}

