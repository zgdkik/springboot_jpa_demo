package com.yanpan.jpademo.controller;


import com.yanpan.jpademo.datasourceguli.service.EduCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author yanpan
 * @since 2022-03-13
 */
@RestController
@RequestMapping("/demo/edu-course")
public class EduCourseController {

    @Autowired
    private EduCourseService eduCourseService;

    @PostMapping("/getInnerConnectData")
    public List<Object> getInnerConnectData(){
        List<Object> list = eduCourseService.getInnerConnectData();
        return list;
    }

    @PostMapping("/editCourseById/{id}")
    public String editCourseTitleById(@PathVariable String id, @RequestParam String title){
        eduCourseService.editCourseById(id,title);
        return "success";
    }

}

