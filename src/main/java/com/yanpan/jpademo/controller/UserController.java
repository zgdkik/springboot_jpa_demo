package com.yanpan.jpademo.controller;

import com.yanpan.jpademo.datasourceone.entity.User1;
import com.yanpan.jpademo.datasourcetwo.entity.User2;
import com.yanpan.jpademo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/getMasterAll")
    public List<User1> getMasterAll(){
        return userService.getMasterAll();
    }

    @PostMapping("/getSlaveAll")
    public List<User2> getSlaveAll(){
        return userService.getSlaveAll();
    }
}
