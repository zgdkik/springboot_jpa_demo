package com.yanpan.jpademo.datasourcetwo.dao;

import com.yanpan.jpademo.datasourcetwo.entity.User2;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;


@Repository
public interface User2Dao extends JpaRepositoryImplementation<User2,Long> {
}
