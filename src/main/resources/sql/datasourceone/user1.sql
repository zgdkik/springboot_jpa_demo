/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : datasourceone

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 14/03/2022 16:20:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user1
-- ----------------------------
DROP TABLE IF EXISTS `user1`;
CREATE TABLE `user1`  (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '姓名',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `version` int(11) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user1
-- ----------------------------
INSERT INTO `user1` VALUES (1, 'Helen Yao', 18, 'helen@qq.com', NULL, '2022-03-02 11:21:11', NULL, NULL);
INSERT INTO `user1` VALUES (2, 'Jack', 125, 'test2@baomidou.com', NULL, '2022-03-04 10:34:33', NULL, NULL);
INSERT INTO `user1` VALUES (3, 'Tom', 28, 'test3@baomidou.com', NULL, NULL, NULL, NULL);
INSERT INTO `user1` VALUES (4, 'Sandy', 21, 'test4@baomidou.com', NULL, NULL, NULL, NULL);
INSERT INTO `user1` VALUES (5, 'Billie', 24, 'test5@baomidou.com', NULL, NULL, NULL, NULL);
INSERT INTO `user1` VALUES (1497161384288243714, 'LisaW', 23, 'LisaW@126.com', NULL, NULL, NULL, NULL);
INSERT INTO `user1` VALUES (1497163321679233026, 'LisaE', 22, 'LisaE@126.com', NULL, NULL, NULL, NULL);
INSERT INTO `user1` VALUES (1497165529007190018, 'LisaA', 24, 'LisaA@126.com', NULL, '2022-02-25 19:04:13', NULL, NULL);
INSERT INTO `user1` VALUES (1498941755396939778, 'LisaB', 29, 'LisaB@126.com', '2022-03-02 16:42:18', '2022-03-02 16:42:18', 1, 0);
INSERT INTO `user1` VALUES (1498945841794084865, 'LisaB', 24, 'LisaB@126.com', '2022-03-02 16:58:32', '2022-03-02 16:58:32', 1, 0);
INSERT INTO `user1` VALUES (1498964858315440129, 'LisaB', 24, 'LisaB@126.com', '2022-03-02 18:14:06', '2022-03-02 18:14:06', 1, 0);
INSERT INTO `user1` VALUES (1498966789075214338, 'LisaB', 24, 'LisaB@126.com', '2022-03-02 18:21:47', '2022-03-02 18:21:47', 1, 0);
INSERT INTO `user1` VALUES (1499564675542495233, 'LisaB', 24, 'LisaB@126.com', '2022-03-04 09:57:34', '2022-03-04 09:57:34', 1, 0);
INSERT INTO `user1` VALUES (1499564991084195842, 'LisaB', 24, 'LisaB@126.com', '2022-03-04 09:58:49', '2022-03-04 09:58:49', 1, 0);
INSERT INTO `user1` VALUES (1499565118708441090, 'LisaB', 24, 'LisaB@126.com', '2022-03-04 09:59:20', '2022-03-04 09:59:20', 1, 0);
INSERT INTO `user1` VALUES (1499573983596441602, 'LisaB', 24, 'LisaB@126.com', '2022-03-04 10:34:33', '2022-03-04 10:34:33', 1, 0);
INSERT INTO `user1` VALUES (1499647158455836673, 'LisaK', 28, 'LisaK@126.com', '2022-03-04 15:25:19', '2022-03-04 15:25:19', 1, 0);
INSERT INTO `user1` VALUES (1499647952274042881, 'LisaK1', 23, 'LisaK1@126.com', '2022-03-04 15:28:29', '2022-03-04 15:28:29', 1, 1);

SET FOREIGN_KEY_CHECKS = 1;
